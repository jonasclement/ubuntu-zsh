#!/usr/bin/env zsh
set -e

# This script will copy dotfiles to their appropriate location, as well as install a default software set.
# It assumes that your SSH key has been copied to ~/.ssh.

DEFAULT_PACKAGES="build-essential chrome-gnome-shell cmake colordiff composer firefox fonts-powerline g++ git gnome-tweak-tool guake imagemagick libavcodec-extra58 libldac microsoft-edge-beta nodejs npm php powerline pulseaudio-modules-bt vim wget xclip zsh"
DEFAULT_SNAPS="code discord gimp telegram-desktop vlc"

echo -n "Creating directories in home dir... "
mkdir -p ~/Git/temp
echo "done"

echo -n "Installing oh-my-zsh... "
git clone --quiet https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh > /dev/null 2>&1
echo "done"

echo -n "Installing default software set... "
wget -O - -o /dev/null https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -u root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm microsoft.gpg
sudo apt-add-repository -y ppa:berglh/pulseaudio-a2dp > /dev/null 2>&1
sudo apt update > /dev/null 2>&1
sudo apt -qq install "$DEFAULT_PACKAGES" > /dev/null 2>&1
sudo snap install "$DEFAULT_SNAPS" > /dev/null 2>&1
echo "done"

echo "Switching shell to zsh... "
chsh -s $(which zsh) > /dev/null 2>&1
echo "done"

echo -n "Configuring Git... "
git config --global user.name "Jonas Clément"
git config --global push.default current
echo -n "Email? "
read EMAIL
git config --global user.email "$EMAIL"
echo " done"

echo -n "Installing zsh-nvm plugin... "
git clone https://github.com/lukechilds/zsh-nvm ~/.oh-my-zsh/custom/plugins/zsh-nvm
echo " done"

echo -n "Copying dotfiles... "
HOME_DIR_FILES=".aliases .vimrc .zshrc"
for file in "$HOME_DIR_FILES"; do
	ln -s $(pwd)/"$file" ~/"$file"
done

# Copying ssh config because changes shouldn't be written to the repo.
cp -f $(pwd)/ssh_config ~/.ssh/config
chmod 600 ~/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
eval `keychain --agents ssh --eval id_rsa`
cp -iR oh-my-zsh/* ~/.oh-my-zsh
cd ~/.oh-my-zsh
git add . > /dev/null 2>&1
git commit -m "Setup-script oh-my-zsh changes" > /dev/null 2>&1
cd ~

echo -n "Installing logiopts... "
git clone --quiet https://github.com/PixlOne/logiops.git ~/Git/temp/logiopts
cd ~/Git/temp/logiopts
mkdir build
cd build
cmake .. > /dev/null 2>&1
make -j2 > /dev/null 2>&1
cp logid.cfg /etc/logid.cfg
echo "done"

echo -n "Performing various last steps... "
setxkbmap -option caps:swapescape
echo "done"

echo -n "Cleaning up... "
rm -rf ~/Git/temp
source ~/.zshrc
echo "done"

echo "done"
echo "Remember to set the correct font in Terminal."
echo "Please log out and log back in."
