syntax enable
color darkblue
hi Normal guibg=NONE ctermbg=NONE

" fix backspace button
set backspace=indent,eol,start
